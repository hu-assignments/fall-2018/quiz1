# Program summary
This is an HMMM program that calculates final position of a falling object 
given time, initial position, and initial velocity, with the gravity constant 
of -10.

First you give the inputs in the order of falling time, initial position, 
initial velocity. Then it calculates and prints the result.

## Important note
During the development, I don't know how it happened but I mistakenly 
committed using my personal GitHub account. I think the system should have 
prohibited the push by another account as this is a private repository. After 
I realized this horrific issue, and how the consequences would be, I was 
really suprised, and immeditely changed email configuration back into 
`b21827095@cs.hacettepe.edu.tr`, then committed again. And now there was two 
contributors to this project (both are my accounts). I don't know if this was 
against the rules, because it was not entirely my fault, GitHub or git should 
have prevented me unknowingly doing this.

Anyway, I deleted the commits (both unknowingly made commit by another 
account and experimantal ones) using <code>git update-ref -d HEAD</code> and 
force pushed using <code>git push --force origin master</code>. "Two 
contributors" issue is solved for me now.
